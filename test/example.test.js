const expect = require('chai').expect;
const assert = require('chai').assert;
const mylib= require('../src/mylib');

describe('Unit testing mylib.js', () =>{


    let myword = undefined;
    let myword2 = undefined;
    before(() => {
        console.log("Before testing");
        myvar=1;
        myword="word";
        myword2="long";
    })
    it("Should return the given words in the same word", ()=>{
        const result = mylib.word(myword,myword2);
        expect(result).to.equal("word long")
    })
    it("Should return the current year, month and date", ()=>{
        const date = mylib.date();
        expect(date).to.equal("2022-2-2")
    })
    it("Assert ", ()=>{
        const date = mylib.date();
        assert(date == "2022-2-2")
    })
    after(()=>{
        console.log("After test ");
    })
})