const { text } = require("express")

module.exports = {
    sum: (a,b) =>{
        return a + b
    },
    word:(word, word2) =>{
        return word.concat(" ",word2);
    },
    date:()=>{
        let today = new Date();
        return today = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }
}